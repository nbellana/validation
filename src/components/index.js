import React, { Component } from "react";
import "./style.css";

class Main extends Component {
  /**
   * Represents Login form with validations
   */
  constructor() {
    super();
    this.state = {
      username: "",
      password: "",
      email: "",
      username_error: "",
      password_error: "",
      email_error: "",
    };
  }

  /**
   * When the input fields are changed
   */

  onChange = (e) => {
    let targetname = [e.target.name];
    this.setState({ [e.target.name]: e.target.value });
    // When input is given ,errors should not display
    if (targetname[0] === "username") {
      this.setState({ username_error: "" });
    } else if (targetname[0] === "password") {
      this.setState({ password_error: "" });
    } else {
      this.setState({ email_error: "" });
    }
  };

  /**
   * Helper function to validate the form upon form submit click
   * @returns true if form validation is succesful, false otherwise
   */
  formValidation = () => {
    // destructuring the state
    let {
      username,
      password,
      email,
      username_error,
      password_error,
      email_error,
    } = this.state;

    let is_username_valid = false;
    let is_password_valid = false;
    let is_email_valid = false;

    // username validations
    if (username === "") {
      username_error = "Please fill this field";
    } else {
      if (!username.match("^(?=[a-zA-Z]{6,9}$)(?!.*[_.]{2})[^_.].*[^_.]$")) {
        username_error = "username is not valid";
      } else {
        username_error = "";
        is_username_valid = true;
      }
    }

    // password validations

    if (password === "") {
      password_error = "Please fill this field";
    } else {
      if (
        !password.match(
          /^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&]).*$/
        )
      ) {
        password_error =
          "Password must contain 1 uppercase,1 lowercase,1 digit,1 special char";
      } else {
        password_error = "";
        is_password_valid = true;
      }
    }

    //email validations
    if (email === "") {
      email_error = "Please fill this field";
    } else {
      if (!email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)) {
        email_error = "Not a valid email";
      } else {
        email_error = "";
        is_email_valid = true;
      }
    }

    this.setState({ username_error, password_error, email_error });
    return is_username_valid && is_password_valid && is_email_valid;
  };

  /**
   * When the user clicks on Submit
   * returns either error in respective fields or form submitted successfully
   */

  onSubmit = (e) => {
    e.preventDefault();
    const isValid = this.formValidation();
    if (isValid === true) {
      //send username and password to server
      alert("Form Submitted");
      console.log(this.state);
      this.setState({ username: "", password: "", email: "" });
    }
  };

  clear = () => {
    this.setState({
      username: "",
      password: "",
      email: "",
      username_error: "",
      password_error: "",
      email_error: "",
    });
  };

  render() {
    const { username, password, email } = this.state;
    return (
      <>
        <div className="main">
          <input type="checkbox" id="chk" aria-hidden="true" />

          <div className="signup">
            <form onSubmit={this.onSubmit}>
              <label htmlFor="chk" aria-hidden="true">
                Sign up
              </label>
              <input
                type="text"
                name="username"
                placeholder="User name"
                value={username}
                required=""
                onChange={this.onChange}
              />
              <div className="errormsgsignup">{this.state.username_error}</div>

              <input
                type="text"
                name="email"
                placeholder="Email"
                value={email}
                required=""
                onChange={this.onChange}
              />
              <div className="errormsgsignup">{this.state.email_error}</div>
              <input
                type="password"
                name="password"
                placeholder="Password"
                value={password}
                required=""
                onChange={this.onChange}
              />
              <div className="errormsgsignup">{this.state.password_error}</div>
              <button> Sign up</button>
            </form>
          </div>

          <div className="login">
            <form onSubmit={this.onSubmit}>
              <label htmlFor="chk" aria-hidden="true" onClick={this.clear}>
                Login
              </label>
              <input
                type="username"
                name="username"
                placeholder="username"
                required=""
                value={username}
                onChange={this.onChange}
              />
              <div className="errormsg">{this.state.username_error}</div>
              <input
                type="password"
                name="password"
                placeholder="Password"
                required=""
                value={password}
                onChange={this.onChange}
              />
              <div className="errormsg">{this.state.password_error}</div>
              <button>Login</button>
            </form>
          </div>
        </div>
      </>
    );
  }
}

export default Main;
